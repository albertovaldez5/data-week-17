# Machine Learning

Machine Learning can be found almost everywhere in the modern tech world.

We have different types of analytics: reporting, analysis and prediction. **Reporting** includes spreadsheets and simple measurements. **Analysis** includes exploratory analysis, visualization, data transformation, generate new data, etc. Then **Prediction** includes dealing with data that is not there but we can assume it fits a model, forecasting, enrich datasets, make decisions. We can do two types of prediction, either values (continuous data) or categories (categorical data).

Machine Learning algorithms are functions with internal parameters that apply labels to data points. They are able to model our decision making process. The simplest ML model is linear regression, where the parameters use the `m` and `b` variables from the line equation.

ML is executed with a different approach, as we use many observations to build the algorithm (training data). By using hundreds to millions of observations, we can get a better view on how we can model the behaviour of real-world scenarios.

In order to learn, the algorithm receives a lots of inputs with the correct answers as well as the incorrect answers, both labeled appropriately.

Supervised learning knows the correct and incorrect answers to the input data. We will have a target variable for the algorithm to optimize on (we know the outcome in advance).

Unsupervised learning doesn&rsquo;t have the answers so it will cluster the data in different groups by looking for patterns, so the data scientist must assign labels manually.

With reinforcement learning we have a small amount of observations, but we use the outcome from the same model to create more input data. For example, a robot using failed attempts at opening a door and collecting the parameters for those attemps, whenever one is successful, then it will store it and be the first to try in the future.

In classification (unsupervised) we look to divide the data into groups. In regression (supervised) we look for a line that sits as close to all observations as possible. They are basically the same problem but with the opposite optimization.


## Supervised Learning

The Model-Fit-Predict paradigm is:

1.  Choosing a model
2.  Fit (train)
3.  Predict

This model is stored as a procedure so you can give it unknown data and get a result immediatly.

When validating, we use data that the model doesn&rsquo;t know about, so we can verify the validity of the results from the training. Then we assess the model by looking at different measures from the test.

The independent variables are features and the dependent variable is called target.


## Linear regression

Linear regression is the simplest model while still being powerful.

We use the m and b in the equation of the line as the features for our model.

y = B0 + B1x

Linear data can have the following trends:

1.  Positive trend
2.  Negative trend
3.  No trend: we have to use a different algorithm

The algorithm measures distances and minimizes different distances between our current line and the observation. In case of many datapoints, the algorithm will optimize for the smaller distance for all the observations.


# Scikit-learn

Scikit-learn provides the models for us. We just have to follow the model-fit-predict paradigm.

We will start importing a dataset from sklearn.

```python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import Path
from sklearn.datasets import make_regression


X, y = make_regression(
    n_samples=20,
    n_features=1,
    random_state=0,
    noise=4,
    bias=100.0,
)
resources = Path('../resources')
```

    

Plot the data.

```python
output = resources / "dataset1.png"
fig, ax = plt.subplots(figsize=(8, 5))
ax.scatter(X, y)
fig.savefig(output)
print("#+attr_html: :width 500px")
print(f"[[{output}]]")
```

<div class="org" id="org3afbc27">

<div id="orga8f05eb" class="figure">
<p><img src="../resources/dataset1.png" alt="dataset1.png" width="500px" />
</p>
</div>

</div>

Now we can import the model from sklearn.

```python
from sklearn.linear_model import LinearRegression

model = LinearRegression()
```

    

Then we fit the data (training in one step).

```python
model.fit(X, y)
print(model.coef_)
print(model.intercept_)
```

    [12.44002424]
    101.89622505659258

Then we make a prediction.

```python
predictions = model.predict(X)
print(predictions)
```

    [100.01333772 106.87419043 114.0717493   91.27125336  89.73886454
     105.79079485 100.61218004  99.34405128 106.04714178 120.48260494
     113.715348   103.40986521 119.98742273 125.12869172 103.68813057
     107.00408037 111.3635528  129.77299077 107.41789443 123.841079  ]

Then we can compare our prediction with the real data.

```python
df = pd.DataFrame({
    "predicted": predictions,
    "real": y,
    "error": predictions - y
})
print(df)
```

         predicted        real     error
    0   100.013338   98.019704  1.993634
    1   106.874190  108.458654 -1.584464
    2   114.071749  107.776544  6.295205
    3    91.271253   90.315201  0.956053
    4    89.738865   92.047965 -2.309101
    5   105.790795  100.144726  5.646069
    6   100.612180  104.371286 -3.759106
    7    99.344051   95.208967  4.135085
    8   106.047142  102.505262  3.541880
    9   120.482605  122.119661 -1.637056
    10  113.715348  112.287600  1.427748
    11  103.409865  107.326140 -3.916275
    12  119.987423  121.444549 -1.457126
    13  125.128692  125.803460 -0.674768
    14  103.688131  104.330672 -0.642542
    15  107.004080  112.026181 -5.022101
    16  111.363553  106.596614  4.766939
    17  129.772991  129.857150 -0.084159
    18  107.417894  113.512862 -6.094967
    19  123.841079  125.422026 -1.580947

Now we can construct our line for plotting.

```python
output = resources / "linear2.png"
x_min = X.min()
x_max = X.max()
y_min = model.predict([[x_min]])
y_max = model.predict([[x_max]])

fig, ax = plt.subplots()

ax.scatter(X, y)
ax.plot([x_min, x_max], [y_min, y_max], c='red')
fig.savefig(output)
print("#+attr_html: :width 500px")
print(f"[[{output}]]")
```

<div class="org" id="org92db8f1">

<div id="orgdf6e0b4" class="figure">
<p><img src="../resources/linear2.png" alt="linear2.png" width="500px" />
</p>
</div>

</div>


## Univariate Linear Regression

If we have many intercepts and features, we will get different coefficients.

The residuals are the differences between the true values of y and the predicted values of y. The problem is that it is scale-dependent.

We can look at the residuals to see how wrong we are.

```python
predictions = model.predict(X)

output = resources / "residuals.png"

fig, ax = plt.subplots(figsize=(8, 5))
ax.scatter(predictions, predictions - y)
plt.hlines(y=0, xmin=predictions.min(), xmax=predictions.max())

fig.savefig(output)
print("#+attr_html: :width 500px")
print(f"[[{output}]]")
```

<div class="org" id="orgae165f8">

<div id="org95cd898" class="figure">
<p><img src="../resources/residuals.png" alt="residuals.png" width="500px" />
</p>
</div>

</div>

We want the residuals to have a nice behaviour, which means that we want values underneath and on top (distributed similarly). This means that the linear regression is not underestimating or underestimating values.

The **r-squared** tells us to which degree we have been able to explain the variance of the target variable using the features. A high **r-square** means that the model explains the relationship very well.

The **mean squared error** measures the average of the squares of the errors or deviations (scale-dependent).


## Quantifying Regression

We will import our dataset. We can use the metrics library from sklearn to get measurements on our model and build the predictions. Then create a linear regression model.

```python
# Import dependencies
from sklearn.datasets import make_regression
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score

# Generate some data
X, y = make_regression(n_samples=20, n_features=1, random_state=0, noise=4, bias=100.0)
model = LinearRegression()
model.fit(X, y)
predicted = model.predict(X)
```

    

Get the r-squared of our regression as well as the mean-squared error. The noisier the data, the lower the r2 value will be, the more samples, the better the r2 will be.

```python
r2 = r2_score(y, predicted)
mse = mean_squared_error(y, predicted)
print("r-squared:", r2)
print("mean-squared error:", mse)
```

    r-squared: 0.903603363418708
    mean-squared error: 11.933040779746149

If we change to a noisier set, we get different results.

```python
X, y = make_regression(n_samples=20, n_features=1, random_state=0, noise=12, bias=100.0)
model = LinearRegression()
model.fit(X, y)
predicted = model.predict(X)
r2 = r2_score(y, predicted)
mse = mean_squared_error(y, predicted)
print("r-squared:", r2)
print("mean-squared error:", mse)
```

    r-squared: 0.3348878392803907
    mean-squared error: 107.39736701771537


## Improving Model with Tests

We can split the dataset with sklearn, which results in a 3 to 1 learn-test data.

```python
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(
    X,
    y,
    random_state=42,
)
print(y.size)
print(y_test.size)
print(y_train.size)
```

    20
    5
    15

We have learned with a different dataset than the testing, one the resulting score is the **r-squared**.

```python
model.fit(X_train, y_train)
score = model.score(X_test, y_test)
print(score)
```

    0.4351627840398349

If we test the model, and the result is good, we can trust it can keep performing well in new observations.


# Linear Regression Example


## Load the Data

```python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

brains = pd.read_csv('../resources/brain.csv')
print(brains.head())
```

       gender  age  size  weight
    0       1    1  4512    1530
    1       1    1  3738    1297
    2       1    1  4261    1335
    3       1    1  3777    1282
    4       1    1  4177    1590


## Assign the data to X and y

```python
X = brains["weight"].values.reshape(-1, 1)
y = brains["size"].values.reshape(-1, 1)

print("Shape: ", X.shape, y.shape)
```

    Shape:  (237, 1) (237, 1)


## Plot the data

```python
output = resources / "brain.png"

fig, ax = plt.subplots(figsize=(8, 5))
ax.scatter(X, y)
ax.set_xlabel("weight of the brain")
ax.set_ylabel("size of the head")
fig.savefig(output)

print("#+attr_html: :width 500px")
print(f"[[{output}]]")
```

<div class="org" id="orgb91cebe">

<div id="orgc6e449c" class="figure">
<p><img src="../resources/brain.png" alt="brain.png" width="500px" />
</p>
</div>

</div>


## Split Learn and Test data

```python
from sklearn.model_selection import train_test_split
X_learn, X_test, y_learn, y_test = train_test_split(X, y, random_state=42)
print(X_learn.size)
print(X_test.size)
print(y_learn.size)
print(y_test.size)
```

    177
    60
    177
    60


## Create and Fit the Model

```python
from sklearn.linear_model import LinearRegression

model = LinearRegression()
model.fit(X_learn, y_learn)
print(model.coef_)
```

    [[2.35989976]]


## Calculate MSE and R2

Mean-squared error and r-squared. Using the original dataset, not split in learn-test.

```python
from sklearn.metrics import mean_squared_error, r2_score

predicted = model.predict(X)
r2 = r2_score(y, predicted)
mse = mean_squared_error(y, predicted)
print("r-squared:", r2)
print("mean-squared error:", mse)
```

    r-squared: 0.6384806219470258
    mean-squared error: 48028.923228939115


## Calculate R2 Score

Calculate r-squared.

```python
score = model.score(X_test, y_test)
print(score)
```

    0.6568088729208812


## Conclusion

The r-squared is too low so we can&rsquo;t trust this model in the future.

```python
y_pred = model.predict(X)
output = resources / "example2lin.png"
fig, ax = plt.subplots(figsize=(8, 5))
# using the model data
ax.scatter(X, y)
ax.plot(X, y_pred, color='red')
fig.savefig(output)
print("#+attr_html: :width 500px")
print(f"[[{output}]]")
```

<div class="org" id="org735193b">

<div id="orgcdd2887" class="figure">
<p><img src="../resources/example2lin.png" alt="example2lin.png" width="500px" />
</p>
</div>

</div>


# Logistic Regression

Logistic regression is a classification algorithm used to discrete set of classes or categories (for example Yes/No, True/False, etc).

We use it as an activation function, so we set a threshold, whichever passes it will be the positive category and whichever doesn&rsquo;t will be part of the negative category (1 and 0 respectively).

We will find the hyperplane that is the farthest away from both groups.


## Logistic regression in python

```python
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.datasets import make_blobs

X, y = make_blobs(centers=2, random_state=42)
print(f"Labels: {y[:10]}")
print(f"Data: {X[:10]}")
```

    Labels: [0 1 0 1 1 0 1 1 0 0]
    Data: [[-2.98837186  8.82862715]
     [ 5.72293008  3.02697174]
     [-3.05358035  9.12520872]
     [ 5.461939    3.86996267]
     [ 4.86733877  3.28031244]
     [-2.14780202 10.55232269]
     [ 4.91656964  2.80035293]
     [ 3.08921541  2.04173266]
     [-2.90130578  7.55077118]
     [-3.34841515  8.70507375]]

```python
output = resources / "logistic1.png"

fig, ax = plt.subplots(figsize=(8, 5))
ax.scatter(X[:, 0], X[:, 1], c=y)
fig.savefig(output)
print("#+attr_html: :width 500px")
print(f"[[{output}]]")
```

<div class="org" id="org3971255">

<div id="org11f145c" class="figure">
<p><img src="../resources/logistic1.png" alt="logistic1.png" width="500px" />
</p>
</div>

</div>

We split the data.

```python
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(
    X,
    y,
    random_state=1,
    stratify=y
)
print(X_train.size)
print(X_test.size)
```

    150
    50

We create a classifier with the logistic regression model.

```python
from sklearn.linear_model import LogisticRegression

classifier = LogisticRegression(
    solver='lbfgs',
    random_state=1
)
classifier.fit(X_train, y_train)
score = classifier.score(X_test, y_test)
print(score)
```

    

Instead of building a line that can be represented a line with an intercept and a slope, we use it as a part of a logistic activation function. It finds a space between the classes and allows us to separate them.

If we introduce new data we can get a classification for it.

```python
new_data = np.array([[2, 6]])
output = resources / "logistic2.png"

fig, ax = plt.subplots(figsize=(8, 5))
ax.scatter(X[:, 0], X[:, 1], c=y)
ax.scatter(new_data[0, 0], new_data[0, 1], c="r", marker="o", s=100)
fig.savefig(output)

print("#+attr_html: :width 500px")
print(f"[[{output}]]")
```

<div class="org" id="org7bb12a9">

<div id="org8ca0177" class="figure">
<p><img src="../resources/logistic2.png" alt="logistic2.png" width="500px" />
</p>
</div>

</div>

So if we evaluate the new observation, we would expect to fit one of the classes.

```python
predictions = classifier.predict(new_data)
print(f"The new point was classified as: {predictions}")
```

    The new point was classified as: [1]


# Logistic Regression Example


## Imports

```python
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt
import pandas as pd
import os

from joblib import dump, load
```


## Load the data

```python
notes = pd.read_csv('../resources/data_banknote_authentication.csv', header=None, names=['variance','skewness','curtosis', 'entropy', 'class'])
print(notes.head())
```

       variance  skewness  curtosis  entropy  class
    0   3.62160    8.6661   -2.8073 -0.44699      0
    1   4.54590    8.1674   -2.4586 -1.46210      0
    2   3.86600   -2.6383    1.9242  0.10645      0
    3   3.45660    9.5228   -4.0112 -3.59440      0
    4   0.32924   -4.4552    4.5718 -0.98880      0


## Assign data

```python
y = notes["class"]
X = notes.drop(columns="class")
X_train, X_test, y_train, y_test = train_test_split(
    X,
    y,
    random_state=1,
    stratify=y
)
print(X_train.shape, X_test.shape)
```

    (1029, 4) (343, 4)


## Init and train the data

```python
classifier = LogisticRegression(
    solver='lbfgs',
    max_iter=200, # upper limit of num of iter solver
    random_state=1
)
classifier.fit(X_train, y_train)
print(classifier)
```

    LogisticRegression(max_iter=200, random_state=1)


## Make predictions

```python
y_pred = classifier.predict(X_test)
results = pd.DataFrame({"Prediction": y_pred, "Actual": y_test}).reset_index(drop=True)
print(results.head())
```

       Prediction  Actual
    0           0       0
    1           0       0
    2           1       1
    3           0       0
    4           0       0


## Evaluate model

```python
train_score = classifier.score(X_train, y_train)
test_score = classifier.score(X_test, y_test)
print("train score:", train_score)
print("test score:", test_score)
```

    train score: 0.9883381924198251
    test score: 0.9941690962099126


## Storing the Model

We can save the trained model for later.

```python
dump(classifier, 'model.joblib')
```


## Reloading the Model

If we reload the model, we don&rsquo;t have to train it, we shouldn&rsquo;t. So we load new data and create a prediction with the model.

```python
model2 = load('model.joblib')
new_data = pd.DataFrame(
    {
        "variance": 3,
        "skewness": 2,
        "curtosis": 1,
        "entropy": 0
    },
    index = [0]
)
predictions = model2.predict(new_data)
print(f"The new point was classified as: {predictions}")
```

    The new point was classified as: [0]


# Confusion Matrix


## Measuring Accuracy

The confusion Matrix will tell us how many times our model was able to predict correct observations, this gives us other important measurements like **sensitivity** that we need to consider while evaluating the models.

|                | Predicted True       | Predicted False      |
| Actually True  | 128 (True Positives) | 5 (False Negatives)  |
| Actually False | 6 (False Positives)  | 111 (True Negatives) |

We build confusion matrices on **test** data.

1.  True or False means that our model and the actual result is what we expected or not.
2.  Positive and Negative refers to the category that the model gave out

|                | Predicted True | Predicted False |
|-------------- |-------------- |--------------- |
| Actually True  | TRUE POSITIVE  | FALSE NEGATIVE  |
| Actually False | FALSE POSITIVE | TRUE NEGATIVE   |


### Accuracy

Accuracy is the number of correct predictions against the total number of predictions.

\begin{equation}
Accuracy = \frac{TP + TN}{TP + TN + FP + FN}
\end{equation}


### Precision

Precision is the measure of how likely is that the prediction is actually true. It tells us the percentage of positive predictions that are correct, how precise a positive prediction is.

\begin{equation}
Precision = \frac{TP}{TP + FP}
\end{equation}


### Sensitivity

Sensitivity is the capability of the model of understanding to which category the data corresponds.

\begin{equation}
Sensitivity = \frac{TP}{TP + FN}
\end{equation}


## Confusion Matrix in Python

```python
from sklearn.metrics import confusion_matrix, classification_report

matrix = confusion_matrix(y_test, y_pred)
true_positive = tp = matrix[0][0]
false_negative = fn = matrix[0][1]
false_positive = fp = matrix[1][0]
true_negative = tn = matrix[1][1]
print(matrix)
```

    [[189   1]
     [  1 152]]

Then we can calculate the accuracy and sensitivity.

```python
print("accuracy:", (tp + tn) / (tp + fn + tn + fp))
print("sensitivity:", tp / (tp + fp))
```

    accuracy: 0.9941690962099126
    sensitivity: 0.9947368421052631

Then we can generate a classification report.

```python
report = classification_report(y_test, y_pred)
print(report)
```

                  precision    recall  f1-score   support
    
               0       0.99      0.99      0.99       190
               1       0.99      0.99      0.99       153
    
        accuracy                           0.99       343
       macro avg       0.99      0.99      0.99       343
    weighted avg       0.99      0.99      0.99       343


# Support Vector Machine

Support Vector Machines (SVMs) are binary classifiers. It is similar to logistic regression, however, the goal of SVM is to find a line that separates the data into two classes. SVM draws a line at the edge of each class, and attempts to maximize the distance between them. It does so by separating the data points with the largest possible margins.

The hyperplanes need the widest equidistant margins possible. This improves classification predictions. The width of the margin is considered the margin of separation.

Support vectors are the data points closest to the hyperplane. They serve as decision boundaries for classification.

However, when there is an outlier, we can use soft margins to accomodate them, as they allow SVMs to make allowances.

In a 3D plane, the hyperplane would need to consider the another dimension to separate the both classes.


## SVMs in practice

We are going to start with a model that has already been scaled.

```python
data = Path('../resources/loans.csv')
df = pd.read_csv(data)
print(df.head())
```

         assets  liabilities  ...  mortgage   status
    0  0.210859     0.452865  ...  0.302682     deny
    1  0.395018     0.661153  ...  0.502831  approve
    2  0.291186     0.593432  ...  0.315574  approve
    3  0.458640     0.576156  ...  0.394891  approve
    4  0.463470     0.292414  ...  0.566605  approve
    
    [5 rows x 6 columns]

Then we select the data and split it for training.

```python
y = df["status"]
X = df.drop(columns="status")

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1, stratify=y)
print(X_test.shape)
print(X_train.shape)
```

    (25, 5)
    (75, 5)

Then we import the model from sklearn and we train it with fit.

```python
from sklearn.svm import SVC
model = SVC(kernel='linear')

model.fit(X_train, y_train)
```

    

Finally we create the predictions.

```python
y_pred = model.predict(X_test)
results = pd.DataFrame({
    "Prediction": y_pred,
    "Actual": y_test
}).reset_index(drop=True)
print(results.head())
```

      Prediction   Actual
    0    approve     deny
    1       deny  approve
    2       deny     deny
    3    approve     deny
    4       deny     deny

Then we get the accuracy score and generate a prediction matrix.

```python
from sklearn.metrics import accuracy_score

accuracy_score(y_test, y_pred)

from sklearn.metrics import confusion_matrix
confusion_matrix(y_test, y_pred)

from sklearn.metrics import classification_report
print(classification_report(y_test, y_pred))
```

                  precision    recall  f1-score   support
    
         approve       0.58      0.58      0.58        12
            deny       0.62      0.62      0.62        13
    
        accuracy                           0.60        25
       macro avg       0.60      0.60      0.60        25
    weighted avg       0.60      0.60      0.60        25

The workflow of a SVM is very similar to a logistic regression:

1.  Select the data (independent and dependent).
2.  Split the data for training.
3.  Create and train the model.
4.  Create predictions.
5.  Validate the model.


# Decision Trees

A decision tree is an algorithm that builds a collection of if-then-else clasues that allow us to find the feature values that are more likely to correspond to a given category, so they can work with multiple categories.

Decision trees can become deep and complex depending on the number of questions that have to be answered. Deep and complex trees tend to overfit the data and don&rsquo;t generalize well.


## Decision Trees in Python

We can visualize a decision tree with `graphviz` and `pydotplus`.

```python
from sklearn import tree
from sklearn.datasets import load_iris
import graphviz
import pydotplus

iris = load_iris()
clf = tree.DecisionTreeClassifier()
clf = clf.fit(iris.data, iris.target)

score = clf.score(iris.data, iris.target)
print(score)
```

    1.0

```python
file = '../resources/tree1.png'
dot_data = tree.export_graphviz(
    clf, out_file=None,
    feature_names=iris.feature_names,
    class_names=iris.target_names,
    filled=True, rounded=True,
    special_characters=True
)
graph = pydotplus.graph_from_dot_data(dot_data)
graph.write_png(file)
print(file)
```

<div class="org" id="org88bab37">

<div id="org19f68af" class="figure">
<p><img src="./../resources/tree1.png" alt="tree1.png" width="500px" />
</p>
</div>

</div>

In this case the first node will use the petal length, if it is less than 2.45, then it will classify the entry as setosa. Because the `gini` is 0.0, we only have a single category in this node, so the node doesn&rsquo;t have any children or other possible categories. However, in the other node, we have a `gini` of 0.5, meaning we have two categories with the same weight.

If we stop at the second node, all the remaining observations are classified as, in this case, versicolor. However, we keep splitting the tree into more nodes until we find nodes where there was only one class left, `gini` is 0.0, these nodes are known as leaves as there are no more branches available.

If we decide to cut the three at some point in the middle and keep the top part, we are left with the more relevant and less granular features at the bottom.


## Aggregation

Anomalies in the training dataset can trick these trees, so we need to use Aggregation to deal with this. It takes a lot of simple algorithms and then takes a consensus, so instead of having a large tree we will have several small trees (weak classifiers) and we put them together to build a strong classifier which we can trust.

We generate an algorithm that decides how to take into consideration the prediction of all single algorithms, such a majority vote (the most voted class wins), or techniques that use the Sensitivity for weighting the results of the vote.


# Random Forest by Hand


## Imports

```python
from matplotlib import pyplot as plt
from sklearn.datasets import make_classification
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report

from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.utils.random import sample_without_replacement
from sklearn.utils import resample
```


## Create Data

We will create a dataset with `make_classification` (like blobs but multi-dimensional), this will help us test our model. In this case we will set it to have 1000 samples, with 10 features from which 5 are useful.

Then we create the `DataFrame` and `split` the data into test and train. We will standarize the data (mean is 0, std is 1) and normalize the numeric values using the `StandardScaler`. Then we use this model to transform the train and test sets.

```python
X, y = make_classification(random_state=42, n_features=10, n_informative=5, n_redundant=0, n_samples=1000)
X = pd.DataFrame(X)
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
scaler = StandardScaler().fit(X_train)
X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)
print(X_test_scaled.shape)
```

    (250, 10)


## Predict

We will do model, fit predict in one step and get a the confusion matrix out of our prediction. Then we print the classification report.

```python
clf = DecisionTreeClassifier().fit(X_train_scaled, y_train)
y_pred = clf.predict(X_test_scaled)
cm = confusion_matrix(y_test, y_pred)
cm_df = pd.DataFrame(
    cm.reshape(2, 2), index=["Actual 0", "Actual 1"],
    columns=["Predicted 0", "Predicted 1"]
)
print(cm_df)
```

              Predicted 0  Predicted 1
    Actual 0          109           20
    Actual 1            9          112

We will note that the testing score is not great as trees are not great at generalizing.

```python
print(classification_report(y_test, y_pred))
print("Training Score:", clf.score(X_train_scaled, y_train))
print("Testing Score:", clf.score(X_test_scaled, y_test))
```

                  precision    recall  f1-score   support
    
               0       0.92      0.84      0.88       129
               1       0.85      0.93      0.89       121
    
        accuracy                           0.88       250
       macro avg       0.89      0.89      0.88       250
    weighted avg       0.89      0.88      0.88       250
    
    Training Score: 1.0
    Testing Score: 0.884


## Bagging

Bagging allows us to give different samples to different trees so that the aggregation of all trees have seen all our dataset together but not individually.

Instead of using a single tree, we will build many programmatically and try to take the best decision possible for each. In this case we will also visualize the score of each tree.

```python
file = "../resources/bagging1.png"
clfs = []
scores = []

for i in range(50):
    X_train_scaled_bootstrap, y_train_bootstrap = resample(X_train_scaled, y_train, random_state=i)

    clf = DecisionTreeClassifier(random_state=i+200).fit(X_train_scaled_bootstrap, y_train_bootstrap)
    clfs.append(clf)

    y_preds = [clf.predict(X_test_scaled) for clf in clfs]
    y_pred = pd.DataFrame(y_preds).median().round()
    score = accuracy_score(y_test, y_pred)
    scores.append(score)

fig, ax = plt.subplots(figsize=(5, 4))
ax.set_title("Scores")
ax.plot(scores)
fig.savefig(file)
print(file)
```

<div class="org" id="org580b2fd">

<div id="org73150b6" class="figure">
<p><img src="../resources/bagging1.png" alt="bagging1.png" />
</p>
</div>

</div>


## Random Forest Pre-built

We can instead use a pre-built Random Forest Classifier.

```python
from sklearn.ensemble import RandomForestClassifier

clf = RandomForestClassifier(random_state=1, n_estimators=50).fit(X_train_scaled, y_train)
print("Previous Score by hand", np.mean(scores))
print("Score Pre-built", clf.score(X_test_scaled, y_test))
```

    Previous Score by hand 0.8984800000000003
    Score Pre-built 0.904

There are other Random Trees Algorithms.

```python
from sklearn.ensemble import ExtraTreesClassifier

clf = ExtraTreesClassifier(random_state=1, n_estimators=50).fit(X_train_scaled, y_train)
print("Train Score:", clf.score(X_train_scaled, y_train))
print("Test Score:", clf.score(X_test_scaled, y_test))
```

    Train Score: 1.0
    Test Score: 0.928


## Ada-Boost Classifier

In AdaBoost, a model is trained then evaluated. After evaluating the errors of the first model, another model is trained. This time, however, the model gives extra weight to the errors from the previous model, so the subsequent models minimize similar errors. This process is repeated until the error rate is minimized.

```python
from sklearn.ensemble import AdaBoostClassifier

clf = AdaBoostClassifier(random_state=1, n_estimators=50, base_estimator=DecisionTreeClassifier(max_depth=2)).fit(X_train_scaled, y_train)

print("Train Score:", clf.score(X_train_scaled, y_train))
print("Test Score:", clf.score(X_test_scaled, y_test))
```


# Choosing an Ensemble Model


## Imports

```python
from matplotlib import pyplot as plt
from sklearn.datasets import make_regression
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report
```


## Loading the Data

```python
df = pd.read_csv('../resources/diabetes.csv')
X = df.drop('Outcome', axis=1)
y = df['Outcome']
target_names = ["negative", "positive"]
print(df.head())
```

       Pregnancies  Glucose  ...  Age  Outcome
    0            6      148  ...   50        1
    1            1       85  ...   31        0
    2            8      183  ...   32        1
    3            1       89  ...   21        0
    4            0      137  ...   33        1
    
    [5 rows x 9 columns]


## Creating a Tester function

We can create a function to automate the process of testing each model.

```python
def model_tester(model, X, y):
    """Returns the classification report and the train and test scores of a given model"""
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
    scaler = StandardScaler().fit(X_train)
    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)
    clf = model.fit(X_train_scaled, y_train)
    y_pred = model.predict(X_test_scaled)
    return (
        classification_report(y_test, y_pred, target_names=target_names),
        clf.score(X_train_scaled, y_train),
        clf.score(X_test_scaled, y_test)
    )
print(model_tester)
```

    <function model_tester at 0x12ff17c20>


## Using the function to compare models

```python
from sklearn.ensemble import AdaBoostClassifier

report, train_score, test_score = model_tester(AdaBoostClassifier(random_state=1), X, y)
print(report, train_score, test_score)
```

                  precision    recall  f1-score   support
    
        negative       0.83      0.85      0.84       123
        positive       0.73      0.70      0.71        69
    
        accuracy                           0.80       192
       macro avg       0.78      0.77      0.78       192
    weighted avg       0.80      0.80      0.80       192
     0.8229166666666666 0.796875

```python
from sklearn.ensemble import RandomForestClassifier

report, train_score, test_score = model_tester(RandomForestClassifier(random_state=1), X, y)
print(report, train_score, test_score)
```

                  precision    recall  f1-score   support
    
        negative       0.83      0.89      0.86       123
        positive       0.78      0.67      0.72        69
    
        accuracy                           0.81       192
       macro avg       0.80      0.78      0.79       192
    weighted avg       0.81      0.81      0.81       192
     1.0 0.8125

```python
from sklearn.ensemble import ExtraTreesClassifier

report, train_score, test_score = model_tester(ExtraTreesClassifier(random_state=1), X, y)
print(report, train_score, test_score)
```

                  precision    recall  f1-score   support
    
        negative       0.82      0.86      0.84       123
        positive       0.73      0.65      0.69        69
    
        accuracy                           0.79       192
       macro avg       0.77      0.76      0.76       192
    weighted avg       0.78      0.79      0.78       192
     1.0 0.7864583333333334


# Feature Selection with Random Forest

What feature selection allows us to do is to determine which features are useful to the model, giving out information about the target variable. We select a subset of features because this allows us to make our dataset smaller (reducing the width) and the models will lose complexity.

We can use the Random Forest models to try to select the best feature at every split. We can grab the complete Random Forest and see which features where used where, so we can get an idea of the importance of them. A feature is important if it appears in most trees and if it appears towards the top of the tree or if it is not a leaf.


## Imports

```python
from matplotlib import pyplot as plt
from sklearn.datasets import make_classification
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
```


## Load Data

```python
df = pd.read_csv('../resources/arrhythmia.csv')
for col in df.columns:
    if df[col].dtype == 'object':
        df[col] = pd.to_numeric(df[col], errors='coerce')

df.drop('J_angle', axis=1, inplace=True)
df.dropna(inplace=True)

X = df.drop('class', axis=1)
y = df['class'] != 1
print(X.shape, y.shape)
```

    (420, 278) (420,)


## Split the Data

```python
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
scaler = StandardScaler().fit(X_train)
X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)

print(X_train_scaled.shape, X_test_scaled.shape)
```

    (315, 278) (105, 278)


## Creating a first Model

We are going to start by making a prediction with a Random Forest Classifier. Then later we may use another model as we will have less features.

```python
clf = RandomForestClassifier(random_state=1, n_estimators=500).fit(X_train_scaled, y_train)
print("Scores (train, test):", clf.score(X_train_scaled, y_train), clf.score(X_test_scaled, y_test))
```

    Scores (train, test): 1.0 0.7142857142857143


## Using the Features attribute

We can use the `feature_importances_` attribute of the Random Forest Model to get the importance of the features as an array.

```python
features = clf.feature_importances_

file = "../resources/features1.png"
fig, ax = plt.subplots(figsize=(5, 3))
ax.bar(x=range(len(features)), height=features)
fig.savefig(file)

print(file)
```

<div class="org" id="org1929da8">

<div id="org87b5fed" class="figure">
<p><img src="../resources/features1.png" alt="features1.png" />
</p>
</div>

</div>

```python
df_f = pd.Series(
    features, index=X.columns
)
df_f.sort_values(inplace=True, ascending=False)
print(df_f.head(10))
```

    Heart_rate              0.080918
    V4_QRSTA                0.043120
    V1_QRSA                 0.033950
    V5_R_wave_amplitude     0.030973
    DII_T_wave_amplitude    0.027136
    V3_JJ_wave_amplitude    0.023354
    V2_JJ_wave_amplitude    0.022617
    V6_T_wave_amplitude     0.022361
    DII_R_wave_width        0.021570
    AVF_Q_wave_amplitude    0.021205
    dtype: float64


## Selecting Features

We don&rsquo;t need all the features so we will use the `feature_selection` module to get ony a few.

The support will be a description of which features are relevant based on the SelectModel fit.

```python
from sklearn.feature_selection import SelectFromModel

sel = SelectFromModel(clf)
sel.fit(X_train_scaled, y_train)
supp = sel.get_support()
print("Support: ", supp[:20])
```

    Support:  [ True False  True  True  True False  True  True  True False False False
     False  True False False  True False False False]


## New Model with the Selected Features

We will re-train the model but using `sel.transform` on our dataset to select only the features we are interested in (those who make the cut).

```python
X_selected_train, X_selected_test, y_train, y_test = train_test_split(sel.transform(X), y, random_state=1)
scaler = StandardScaler().fit(X_selected_train)
X_selected_train_scaled = scaler.transform(X_selected_train)
X_selected_test_scaled = scaler.transform(X_selected_test)
print(X_selected_train_scaled.shape, X_selected_test_scaled.shape)
```

    /Users/albertovaldez/.pyenv/versions/3.7.13/lib/python3.7/site-packages/sklearn/base.py:444: UserWarning: X has feature names, but SelectFromModel was fitted without feature names
      f"X has feature names, but {self.__class__.__name__} was fitted without"
    (315, 108) (105, 108)


## Evaluating the new Model

```python
clf = LogisticRegression().fit(X_train_scaled, y_train)
print("Scored (train, test):", clf.score(X_train_scaled, y_train), clf.score(X_test_scaled, y_test))
```

    Scored (train, test): 0.9523809523809523 0.7142857142857143
